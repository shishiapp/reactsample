import { combineReducers } from 'redux'
import * as Actions from './actions'

function selectedTriplet(state = null, action) {

    let newState = Object.assign({}, state);
    switch (action.type) {
        case Actions.UPDATE_SELECTED_TRIPLET:
            newState = {};
            return newState;
        default:
            return state;
    }
}


const appReducers = combineReducers({
    selectedTriplet
});

export default appReducers