import React, {Component} from 'react';
import {Spin, List, Card, Layout} from 'antd';
import {ShiShiSdk, ColorSwatchCanvas, ColorSwatch} from 'shishisdk';
import unavailable from './unavailable.jpg';
import * as Actions from './actions';
import {connect} from "react-redux";
import {isMobile} from "react-device-detect";

class Swatches extends Component {

    constructor(props) {
        super(props);

        this.state = {
            triplets: [],
            swatchCanvasInitialized: false,
            loading: false
        };

    }


    componentDidMount() {

        let ids = [87, 88, 89, 90, 91, 92, 93, 94, 95, 96];


        ShiShiSdk.getInstance().getTripletsOfProductItems(ids, (triplets, more) => {

            triplets.unshift(null);
            this.setState({triplets: triplets, loading: false});

        }, error => {});
    }

    swatchCanvasInitialized() {
        this.setState({swatchCanvasInitialized: true});
    }

    selected(triplet) {
        let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();
        if (selectedTriplet == null) {
            if (triplet == null) {
                return {borderColor: 'black'};
            }
        } else {
            if (triplet != null && selectedTriplet[0].tryOnGroup === triplet[0]) {
                return {borderColor: 'black'};
            }
        }

        return {borderColor: 'white'};
    }


    handleSwatchClick(e, triplet) {


        if (triplet != null) {

            let tripletOfLook = ShiShiSdk.getInstance().createTripletOfLookForReplacement(triplet);


            ShiShiSdk.getInstance().removeAllMakeup();
            ShiShiSdk.getInstance().putTripletInCurrentLook(tripletOfLook, true);
            ShiShiSdk.getInstance().setSelectedTripletOfLook(tripletOfLook);
            ShiShiSdk.getInstance().syncAllMakeup();
            this.props.dispatch(Actions.updateSelectedTriplet());

        } else {
            let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();
            if(selectedTriplet) {
                ShiShiSdk.getInstance().removeTripletFromCurrentLook(selectedTriplet);
                this.props.dispatch(Actions.updateSelectedTriplet());
                ShiShiSdk.getInstance().syncAllMakeup();
            }
        }
    }


    renderSwatchCell(swatchCanvas, triplet) {
        if (triplet == null) {
            return <img src={unavailable} style={{width: '100%', height: '100%'}}/>;

        } else {
            return <ColorSwatch canvas = {swatchCanvas} tryOnItems = {triplet[0].tryOnItems} style={{width: '100%', height: '100%'}}/>;
        }
    }


    render() {
        return (

            <div>
                <ColorSwatchCanvas ref={ref => this.swatchCanvas = ref} width={44} height={44}
                                   finishInit={() => this.swatchCanvasInitialized()}/>

                {isMobile ? (

                    <div className={'mobile-swatch-wrapper'}>
                        {this.state.triplets.map((item) => (
                            <Card className={'swatch-card'} style={this.selected(item)} hoverable
                                  onClick={((e) => this.handleSwatchClick(e, item))}>
                                {this.renderSwatchCell(this.swatchCanvas, item)}
                            </Card>
                        ))}
                        {this.state.loading && <Spin className="demo-loading"/>}
                    </div>
                ) : (

                    <List
                        grid={{gutter: 16, xs: 4, sm: 6, md: 3, lg: 4, xl: 5, xxl: 6}}
                        dataSource={this.state.triplets.map((triplet, index) => Object.assign({}, {triplet: triplet}, {key: index}))}

                        renderItem={item => (
                            <List.Item>
                                <Card className={'swatch-card'} style={this.selected(item.triplet)} hoverable
                                      onClick={((e) => this.handleSwatchClick(e, item.triplet))}>
                                    {this.renderSwatchCell(this.swatchCanvas, item.triplet)}
                                </Card>
                            </List.Item>
                        )}>
                        {this.state.loading && <Spin className="demo-loading"/>}
                    </List>
                )}
            </div>);
    }
}


const mapStateToProps = store => {
    return {
        selectedTriplet: store.selectedTriplet
    }
};

export default Swatches = connect(mapStateToProps)(Swatches);