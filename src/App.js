import React, {Component} from 'react';
import './App.css';
import {Provider} from 'react-redux'
import appReducers from './reducers'
import {createStore} from 'redux'
import Root from './Root';


class App extends Component {

    store = createStore(appReducers);

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }


    render() {
        return (
            <Provider store={this.store}>
                <Root/>
            </Provider>

        );
    }
}

export default App;
