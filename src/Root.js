import React, {Component} from 'react';
import ReactDOM from "react-dom";
import {Layout, Row, Col, Spin, Button} from 'antd';
import 'antd/dist/antd.css';
import Swatches from "./Swatches";
import {isMobile} from 'react-device-detect';

import {ShiShiSdk, ShiShiZoidComponent} from "shishisdk";

const ShiShiComponent = ShiShiZoidComponent.driver('react', {
    React: React,
    ReactDOM: ReactDOM
});


class Root extends Component {

    constructor(props) {
        super(props);

        this.onInitialized = this.onInitialized.bind(this);
        this.handleBeautifyButtonClick = this.handleBeautifyButtonClick.bind(this);
        this.handleToggleButtonClick = this.handleToggleButtonClick.bind(this);
        this.handleCaptureButtonClick = this.handleCaptureButtonClick.bind(this);
        this.handleDownloadButtonClick = this.handleDownloadButtonClick.bind(this);
        this.handleTryOnButtonClick = this.handleTryOnButtonClick.bind(this);

        this.state = {
            initialized: false,
            sdkInitialized: false,
            beautify: true,
            toggle: true,
            captureImage: false,
            downloadImage: false,
            tryOn: false
        };

    }


    componentDidMount() {

        ShiShiSdk.getInstance().initialize("https://sg.shishiapp.com", true, () => {
            console.log("shishi sdk initialized");
            this.setState({sdkInitialized: true});
        });
    }

    onError(error) {
        window.alert(error);
    }

    onInitialized(makeupComponent) {
        this.setState({initialized: true});
        ShiShiSdk.getInstance().setMakeupComponent(makeupComponent);
        ShiShiSdk.getInstance().syncAllMakeup();
        ShiShiSdk.getInstance().setBeautify(2, 2);
    }

    handleBeautifyButtonClick() {
        let beautifyEnabled = !this.state.beautify;

        this.setState({beautify: beautifyEnabled});

        if (beautifyEnabled) {
            ShiShiSdk.getInstance().setBeautify(2, 2);
        } else {
            ShiShiSdk.getInstance().setBeautify(1, 0);
        }
    }


    handleToggleButtonClick() {
        let toggle = !this.state.toggle;
        ShiShiSdk.getInstance().toggleAllMakeup(toggle);
        this.setState({toggle: toggle});
    }

    handleCaptureButtonClick() {
        ShiShiSdk.getInstance().captureImage(true);
        this.setState({captureImage: true, downloadImage: false});
    }

    handleDownloadButtonClick() {
        ShiShiSdk.getInstance().downloadImage("downloadedImage.jpg");
        ShiShiSdk.getInstance().captureImage(false);
        this.setState({captureImage: false, downloadImage: true});
    }

    handleTryOnButtonClick() {
        if (this.state.tryOn) {
            ShiShiSdk.getInstance().setMakeupComponent(null);
            this.setState({tryOn: false});
        } else {
            this.setState({tryOn: true, initialized: false, toggle: true, beautify: true, captureImage: false, downloadImage: false});
        }
    }

    updateButtons() {
        let toggleButton =
            (<Button onClick={this.handleToggleButtonClick}>Toggle ON </Button>);
        if (this.state.toggle) {
            toggleButton =
                (<Button onClick={this.handleToggleButtonClick}>Toggle OFF</Button>);
        }


        let captureButton =
            (<Button onClick={this.handleCaptureButtonClick}>Capture Pic</Button>);

        if (this.state.captureImage) {
            captureButton = (<Button onClick={this.handleDownloadButtonClick}>Download Pic</Button>);
        }


        let beautifyButton = (<Button onClick={this.handleBeautifyButtonClick}>Beautify</Button>);

        if (this.state.beautify) {
            beautifyButton = (<Button onClick={this.handleBeautifyButtonClick}>Raw</Button>);
        }


        return (<Button.Group className={'makeup-action-bar'}>
            {beautifyButton}
            {captureButton}
            {toggleButton}
        </Button.Group>);


    }

    render() {
        return (

            <Layout>

                {isMobile ? (

                    this.state.tryOn ? (

                            <div className={'component-container'}
                                 style={{position: 'absolute', width: '100%', top: 0, bottom: 0}}>

                                <ShiShiComponent url='https://sdk.shishiapp.com/component.html'
                                                 onInitialized={this.onInitialized}
                                                 onError={this.onError}/>


                                <Button className={'close-tryon-button'}
                                        onClick={this.handleTryOnButtonClick}>x</Button>

                                <Layout style={{width: '100%', position: 'absolute', bottom: 0, backgroundColor: 'transparent'}}>
                                    {this.state.sdkInitialized ?
                                        (<Swatches/>) : null
                                    }
                                </Layout>


                                {this.state.initialized ? null :
                                    (
                                        <Spin className={'makeup-component-spin'}/>
                                    )
                                }
                            </div>
                        ) : (

                            <div style={{backgroundColor:'#fff', height: '100vh', position: 'relative'}}>
                            <Button className={'tryon-button'} onClick={this.handleTryOnButtonClick}>Virtual Try
                                On</Button>

                            </div>
                    )

                ) : (
                    <Row type="flex">
                        <Col xs={24} sm={24} md={24} lg={16} xl={16}>

                            <Layout style={{background: '#fff', height: '100%'}}>
                                {this.state.sdkInitialized ?
                                    (<Swatches/>) : null
                                }
                            </Layout>

                        </Col>


                        <Col xs={24} sm={24} md={24} lg={8} xl={8} style={{background: '#fff', height: 480}}>


                            {this.state.tryOn ? (

                                <div className={'component-container'}
                                     style={{marginTop: "24px", position: 'relative', width: 360, height: 480}}>

                                    <ShiShiComponent url='https://sdk.shishiapp.com/component.html'
                                                     onInitialized={this.onInitialized}
                                                     onError={this.onError}/>

                                    <Button className={'close-tryon-button'}
                                            onClick={this.handleTryOnButtonClick}>x</Button>

                                    {this.updateButtons()}


                                    {this.state.initialized ? null :
                                        (
                                            <Spin className={'makeup-component-spin'}/>
                                        )
                                    }
                                </div>) : (
                                <Button className={'tryon-button'} onClick={this.handleTryOnButtonClick}>Virtual Try
                                    On</Button>)
                            }
                        </Col>

                    </Row>
                )}


            </Layout>
        );
    }
}

export default Root;
