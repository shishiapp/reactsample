export const UPDATE_LOOK = 'UPDATE_LOOK';
export const UPDATE_SELECTED_TRIPLET = 'UPDATE_SELECTED_TRIPLET';


export function updateLook() {
    return {
        type: UPDATE_LOOK
    }
}

export function updateSelectedTriplet() {
    return {
        type: UPDATE_SELECTED_TRIPLET
    }
}