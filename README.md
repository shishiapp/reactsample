# 1. Introduction
ShiShi SDK supports virtual makeup try-on for eye (eyeshadow, eyeliner, mascara), lip (lipstick, lipgloss, lip liner), face (foundation, blush, contour/highlight) and eyebrow products. 

It fetches try-on specific data, such as color, finish and application styles from the VTO server.


## 1.1 Data Model
The SDK's core data model is a three level `Triplet`: `Concept` -> `ProductItem` -> `TryOnGroup`. 

`Concept` represents a group of products, of the same category, brand, and likely the same packaging. The products of a `Single Try-On` concept has only one shade (`TryOnItem`), otherwise they have multiple (the same number of) `TryOnItem`s.

`ProductItem` is a product, or a SKU, with unique product ID.

`TryOnGroup` is one or a group of shades (`TryOnItem`s) that are applied together according to `MakeupStyle`s.

For makeup rendering, a similar three level `TripletOfLook`: `Concept` -> `ProductItem` -> `TryOnGroupOfLook` is derived from `Triplet`.

Here is a conceptual diagram of the data model:

![Conceptual diagram of the data model](./data-model.jpg?raw=true)

## 1.2 SDK Structure
 
`shishisdk` is a npm package consists of: 
 
`ShiShiSdk`: a singleton that fetches information from VTO server, and prepares data for rendering.
 
`ColorSwatch`: a React Component for rendering color swatch.
 
`ProductCategory`: includes definitions of available makeup categories.
 
`ShiShiZoidComponent`: a cross domain component that loads a web page into an iframe, where makeup rendering is performed.
 
 


# 2. Step-by-step tutorial

## Bootstrap project with Create React App

Create React App (https://github.com/facebook/create-react-app).
Run the app in development mode with `npm start`, and open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## 2.1 Import shishisdk

Install the latest version from npm:
```
npm install shishisdk --save
```

Import ShiShi classes/Components where they are needed:
```JSX
import {ShiShiSdk, ColorSwatch, ColorSwatchCanvas, ProductCategory, ShiShiZoidComponent} from 'shishisdk';
```


## 2.2 Inititalize ShiShiSdk

With the URL to the VTO server, argument `true` if we only need the basic function, and a callback which triggers rendering of components that use the SDK.

```JSX
ShiShiSdk.getInstance().initialize("https://sg.shishiapp.com", true, () => {
    this.setState({sdkInitialized: true});
});
```

## 2.3 Embed ShiShiZoidComponent

Create React component from ShiShiZoidComponent:
```JSX
const ShiShiComponent = ShiShiZoidComponent.driver('react', {
    React: React,
    ReactDOM: ReactDOM
});
```

Render it:

```JSX
<ShiShiComponent url='https://sdk.shishiapp.com/component.html'
    onInitialized={this.onInitialized}
    onError={this.onError}
```


`url` points to the ShiShi Zoid Component (`https://[host domain]/component.html`) in production.
 
`onInitialized` is called when the component finishes initialization. We need to pass the returned object to ShiShiSdk

`onError` is called when the component can't function due to missing support for `camera` or `webgl`.

The makeup component needs to be passed to the SDK:

```
onInitialized(makeupComponent) {
    ShiShiSdk.getInstance().setMakeupComponent(makeupComponent);
}
```

and set to null when the tryon component is closed:

```
handleCloseButtonClick() {
    ShiShiSdk.getInstance().setMakeupComponent(null);
}
```

## 2.4 Display color swatches

Load `triplets` of the requested products

```JSX
ShiShiSdk.getInstance().getTripletsOfProductItems(ids, (triplets, more) => {
    triplets.unshift(null);
    this.setState({triplets: triplets, loading: false});
}, error => {});
```

Use a invisible `ColorSwatchCanvas` for drawing swatches into image which will be displayed in `ColorSwatch`

```JSX
<ColorSwatchCanvas ref={ref => this.swatchCanvas = ref} width={44} height={44} finishInit={() => this.swatchCanvasInitialized()}/>
```

Need to trigger rerender when `ColorSwatchCanvas` is initialized:

```JSX
swatchCanvasInitialized() {
    this.setState({swatchCanvasInitialized: true});
}
```

Now we can display each triplet in a List Item (here we used `List` from `antd`), with the `swatchCanvas` as property

```JSX
<List.Item>
    <Card className={'swatch-card'} style={this.selected(item.triplet)} hoverable
        onClick={((e) => this.handleSwatchClick(e, item.triplet))}>
        {this.renderSwatchCell(this.swatchCanvas, item.triplet)}
    </Card>
</List.Item>
```

`ColorSwatch` takes aforementioned `ColorSwatchCanvas` and the `tryOnItems` of the triplet
```JSX
<ColorSwatch canvas = {swatchCanvas} tryOnItems = {triplet[0].tryOnItems}/>
```
`selected(triplet)` highlights selected triplet


## 2.5 Handle Swatch click `handleSwatchClick(triplet)`

When user clicks a swatch, we get the associated triplet, however it doesn't include all the style information for rendering,
so we first need to create a `tripletOfLook`:

```JSX
let tripletOfLook = ShiShiSdk.getInstance().createTripletOfLookForReplacement(triplet);
```

Then we remove everything from current look, and add the newly created `tripletOfLook` and tell the sdk to sync the look to the makeup component.
```JSX
ShiShiSdk.getInstance().removeAllMakeup();
ShiShiSdk.getInstance().putTripletInCurrentLook(tripletOfLook, true);
ShiShiSdk.getInstance().syncAllMakeup();
```



Then we let ShiShiSdk know the `tripletOfLook` is currently selected
```JSX
ShiShiSdk.getInstance().setSelectedTripletOfLook(tripletOfLook);
```

At last, we notify the app of the change which will trigger rerender of relevant components

```JSX
this.props.dispatch(Actions.updateSelectedTriplet());
```

If the user clicks the first swatch (`null` triplet), we remove the currently selected triplet
(or simply removing all triplets of current look), then notify the app

```JSX
let selectedTriplet = ShiShiSdk.getInstance().getSelectedTripletOfLook();
if(selectedTriplet) {
    ShiShiSdk.getInstance().removeTripletFromCurrentLook(selectedTriplet);
    ShiShiSdk.getInstance().syncAllMakeup();    
    this.props.dispatch(Actions.updateSelectedTriplet());
}
```


## 2.7 Build project for production
 
### 2.7.1 `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### 2.7.2 `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
